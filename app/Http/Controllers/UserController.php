<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\UserService;
use Illuminate\Http\Response;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(UserService $userService)
    {
        $userDetails = $userService->getAllUsers();
        return response()->json(['status'=>"success","data" => $userDetails], 200, []);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, UserService $userService)
    {

        if($request->isJson()){
            $formData = $request->json()->all();     
            $tokenDataJson = json_decode(json_encode($formData['data'], JSON_PRESERVE_ZERO_FRACTION), false, 512, JSON_BIGINT_AS_STRING);      
        }else{
            return response()->json(['status'=>"failure","message" => "Please provide json data"], 500, []);
        }
        try{
          $userService->saveUser($tokenDataJson);
        }catch(Exception $e){
          //Logging
        }  
        return response()->json(['status'=>"success"], 201, []);

        
    }

    // public function respond($data, $headers = []){
    //     return Response::json($data, $this->statusCode, $headers);
    // }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($userId, UserService $userService)
    {
        $userDetails = $userService->getUser($userId);
        return response()->json(['status'=>"success","data" => $userDetails], 200, []);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $userId, UserService $userService)
    {
        if($request->isJson()){
            $formData = $request->json()->all();     
            $tokenDataJson = json_decode(json_encode($formData['data'], JSON_PRESERVE_ZERO_FRACTION), false, 512, JSON_BIGINT_AS_STRING);      
        }else{
            return response()->json(['status'=>"failure","message" => "Please provide json data"], 500, []);
        }
        try{
          $userService->updateUser($tokenDataJson,$userId);
        }catch(Exception $e){
          //Logging
        }  
        return response()->json(['status'=>"success"], 201, []);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($userId, UserService $userService)
    {
        $userService->deleteUser($userId);
        return response()->json(['status'=>"success"], 200, []);
    }

    public function getAccountDetails(UserService $userService)
    {
        $userDetails = $userService->accountDetails();
        return response()->json(['status'=>"success","data" => $userDetails], 200, []);
    }

    
}
