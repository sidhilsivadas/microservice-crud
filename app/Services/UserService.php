<?php

namespace App\Services;

use App\Repositories\UserRepository;
use GuzzleHttp\Client as HttpClient;

class UserService
{

    public static function saveUser($formData){
        $data = UserRepository::saveUser($formData); 
        return $data;
    }

    public static function updateUser($formData,$id){
        $data = UserRepository::updateUser($formData,$id);      
        return $data;
    }

    public static function getUser($userId){
        $data = UserRepository::getUser($userId);  
        //dd($data['bank_account_details']);
        if(!empty($data->bank_account_details))
         $data->bank_account_details = json_decode($data->bank_account_details);  
        return $data;
    }

    public static function getAllUsers(){
        $data = UserRepository::getAllUsers();    
        $data = $data->map(function ($temp, $key) {
        $temp->bank_account_details = json_decode($temp->bank_account_details);
          return $temp;
        });
        return $data;
    }

    public static function deleteUser($userId){
        $data = UserRepository::deleteUser($userId);  
        return $data;
    }

    public static function accountDetails(){
        $data = UserRepository::getAllUsers();    
        $data = $data->map(function ($temp, $key) {
        $temp->bank_account_details = json_decode($temp->bank_account_details);
        $temp->total_bank_account_balance = collect($temp->bank_account_details)->sum('balance');
        unset($temp->bank_account_details);
          return $temp;
        });

        return $data;
    }


    
}
